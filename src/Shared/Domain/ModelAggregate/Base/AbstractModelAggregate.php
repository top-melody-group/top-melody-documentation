<?php

namespace Shared\Domain\ModelAggregate\Base;

abstract class AbstractModelAggregate
{

    /**
     * @var string[]
     */
    protected array $errors = [];

    protected function addError(string $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * @return string[]
     */
    public function getValidationErrors(): array
    {
        return $this->errors;
    }
}