<?php

namespace Shared\Domain\ModelAggregate\Base;

interface ModelAggregateInterface
{
    public function validate(): bool;

    /**
     * @return string[]
     */
    public function getValidationErrors(): array;

    public function save(): bool;
}