<?php

namespace Shared\Domain\Enum;

enum ApiOperationTypeEnum: string
{
    case UserTrackList = 'userTrackList';
    case LastTrackList = 'lastTrackList';
    case TrackDownload = 'download';
    case TrackStream = 'trackStream';
}