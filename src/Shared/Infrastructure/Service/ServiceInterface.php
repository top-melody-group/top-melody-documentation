<?php

namespace Shared\Infrastructure\Service;

interface ServiceInterface
{
    public function service(): mixed;
}