<?php

namespace SDK;

class Response
{
    public ?bool $success = null;
    public ?array $trackList = null;
    public ?array $errors = null;
}