<?php

namespace SDK;

class Request
{
    public ?int $limit = null;
    public ?string $userId = null;
}