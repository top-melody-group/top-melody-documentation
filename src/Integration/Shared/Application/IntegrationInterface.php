<?php

namespace Integration\Shared\Application;

use DateTimeInterface;
use http\Encoding\Stream;
use Integration\Shared\Domain\DTO\TrackDTO;

interface IntegrationInterface
{
    /**
     * @return TrackDTO[]
     */
    public function getTrackListByDates(DateTimeInterface $dateFrom, DateTimeInterface $dateTo): array;

    /**
     * @return TrackDTO[]
     */
    public function getTrackListByUserUrl(string $userUrl): array;

    public function getTrackStream(string $downloadUrl): Stream;

    public function downloadTrack(string $downloadUrl, string $path): void;
}