<?php

namespace Integration\Shared\Domain\ApiOperation;

use Shared\Domain\Enum\ApiOperationTypeEnum;
use Shared\Domain\Enum\IntegrationEnum;
use Shared\Infrastructure\Service\ServiceInterface;
use Exception;
use Throwable;

class ApiOperation implements ServiceInterface
{

    private const MESSAGE_NOT_A_SERVICE = 'Класс не является сервисом';

    public function __construct(
        private ?IntegrationEnum $integration,
        private ?ApiOperationTypeEnum $type,
    
        private ?string $requestBuilderClass,
        private ?string $senderClass,
        private ?string $errorsAnalyzerClass,
        private ?string $resultBuilderClass,

        private array $requestBuilderDependencies = [],
        private array $senderDependencies = [],
        private array $errorsAnalyzerDependencies = [],
        private array $resultBuilderDependencies = [],
    ) {}

    public function service(): ApiOperationResult
    {
        $operationResult = new ApiOperationResult();
        
        try {            
            $request = $this->callOperation($this->requestBuilderClass, ...$this->requestBuilderDependencies);
            $response = $this->callOperation($this->senderClass, $request, ...$this->senderDependencies);
            
            if ($this->errorsAnalyzerClass) {
                $errors = $this->callOperation($this->errorsAnalyzerClass, $response, ...$this->errorsAnalyzerDependencies);
                $operationResult->addErrors($errors);
            }
            
            if ($operationResult->getSuccess()) {
                $result = $this->callOperation($this->resultBuilderClass, $response, ...$this->resultBuilderDependencies);
                $operationResult->setResult($result);
            }
        } catch (Throwable $exception) {
            $operationResult->addError($exception->getMessage());
        } finally {
            $integration = $this->integration;
            $type = $this->type;
            $request = $request ?? null;
            $response = $response ?? null;

            // log
        }
        
        return $operationResult;
    }

    /**
     * @throws Exception
     */
    private function callOperation(?string $class, mixed ...$dependencies): mixed
    {
        $instance = new $class(...$dependencies);
        if (!$instance instanceof ServiceInterface) {
            throw new Exception(self::MESSAGE_NOT_A_SERVICE);
        }

        return $instance->service();
    }
}