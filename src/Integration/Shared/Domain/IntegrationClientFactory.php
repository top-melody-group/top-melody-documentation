<?php

namespace Integration\Shared\Domain;

use Exception;
use SDK\Client;
use Shared\Domain\Enum\IntegrationEnum;
use Shared\Infrastructure\Service\ServiceInterface;

class IntegrationClientFactory implements ServiceInterface
{

    private const MESSAGE_NOT_VALID_MAPPING = 'Не найден маппинг клиента к интеграции %s';

    public function __construct(
        private IntegrationEnum $enum
    ) {}

    /**
     * @throws Exception
     */
    public function service(): mixed
    {
        return $this->getClientByEnum();
    }

    /**
     * @throws Exception
     */
    private function getClientByEnum(): mixed
    {
        $class = match ($this->enum) {
            IntegrationEnum::PromoDJ => Client::class,
            default => throw new Exception(sprintf(self::MESSAGE_NOT_VALID_MAPPING, $this->enum->value)),
        };

        return new $class();
    }
}