<?php

namespace Integration\Shared\Domain\DTO;

use Shared\Domain\Enum\IntegrationEnum;

class TrackDTO
{
    public ?string $name = null;
    public ?float $duration = null;
    public ?string $downloadUrl = null;
    public ?IntegrationEnum $integration = null;
    public ?array $authorNames = null;
}