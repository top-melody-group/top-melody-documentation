<?php

namespace Integration\PromoDJ\Application;

use DateTimeInterface;
use http\Encoding\Stream;
use Integration\PromoDJ\Domain\OperationAggregate\GetTrackListByUserUrl;
use Integration\Shared\Application\IntegrationInterface;

class Integration implements IntegrationInterface
{

    public function getTrackListByDates(DateTimeInterface $dateFrom, DateTimeInterface $dateTo): array
    {
        // TODO: Implement getTrackListByDates() method.
    }

    public function getTrackListByUserUrl(string $userUrl): array
    {
        $service = new GetTrackListByUserUrl($userUrl);
        return $service->service();
    }

    public function getTrackStream(string $downloadUrl): Stream
    {
        // TODO: Implement getTrackStream() method.
    }

    public function downloadTrack(string $downloadUrl, string $path): void
    {
        // TODO: Implement downloadTrack() method.
    }
}