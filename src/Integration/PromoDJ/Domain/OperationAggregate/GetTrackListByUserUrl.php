<?php

namespace Integration\PromoDJ\Domain\OperationAggregate;

use Integration\Promodj\Domain\Operation\GetTrackListByUserUrl\ErrorsAnalyser;
use Integration\Promodj\Domain\Operation\GetTrackListByUserUrl\RequestBuilder;
use Integration\Promodj\Domain\Operation\GetTrackListByUserUrl\ResultBuilder;
use Integration\Promodj\Domain\Operation\GetTrackListByUserUrl\Sender;
use Integration\Shared\Domain\DTO\TrackDTO;
use Integration\Shared\Domain\ApiOperation\ApiOperation;
use Integration\Shared\Domain\ApiOperation\ApiOperationBuilder;
use Integration\Shared\Domain\IntegrationClientFactory;
use SDK\Client;
use Shared\Domain\Enum\IntegrationEnum;
use Shared\Infrastructure\Service\ServiceInterface;

class GetTrackListByUserUrl implements ServiceInterface
{

    public function __construct(
        private string $userUrl,
    ) {}

    /**
     * @return TrackDTO[]
     */
    public function service(): array
    {
        $searchResult = $this->getSearchApiOperation()->service();

        //format result
        return [];
    }

    private function getSearchApiOperation(): ApiOperation
    {
        return ApiOperationBuilder::create()
            ->setRequestBuilder(RequestBuilder::class, $this->userUrl)
            ->setSender(Sender::class, $this->getClient())
            ->setErrorsAnalyzer(ErrorsAnalyser::class)
            ->setResultBuilder(ResultBuilder::class)
            ->service();
    }

    private function getClient(): Client
    {
        $factory = new IntegrationClientFactory(IntegrationEnum::PromoDJ);
        return $factory->service();
    }
}