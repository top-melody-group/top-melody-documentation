<?php

namespace Integration\Promodj\Domain\Operation\GetTrackListByUserUrl;

use SDK\Response;
use Shared\Infrastructure\Service\ServiceInterface;

class ErrorsAnalyser implements ServiceInterface
{

    public function __construct(
        private Response $response,
    ) {}

    /**
     * @return string[]
     */
    public function service(): array
    {
        return $this->response->errors ?? [];
    }
}