<?php

namespace Integration\Promodj\Domain\Operation\GetTrackListByUserUrl;

use SDK\Request;
use Shared\Infrastructure\Service\ServiceInterface;

class RequestBuilder implements ServiceInterface
{
    public function __construct(
        private string $userUrl
    ) {}

    public function service(): Request
    {
        return $this->getDTO();
    }

    private function getDTO(): Request
    {
        $dto = new Request();
        $dto->limit = 0;
        $dto->userId = $this->getUserId();

        return $dto;
    }

    private function getUserId(): string
    {
        $parts = explode('/', $this->userUrl);
        return array_pop($parts);
    }
}