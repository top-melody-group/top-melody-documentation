<?php

namespace Integration\Promodj\Domain\Operation\GetTrackListByUserUrl;

use Exception;
use SDK\Response;
use Shared\Infrastructure\Service\ServiceInterface;

class ResultBuilder implements ServiceInterface
{

    public function __construct(
        private Response $response,
    ) {}

    /**
     * @throws Exception
     */
    public function service(): array
    {
        return $this->getTrackList();
    }

    private function getTrackList(): array
    {
        return $this->response->trackList;
    }
}